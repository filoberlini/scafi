package lib

import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import it.unibo.scafi.simulation.gui.model.implementation.SensorEnum

/**
  * @author Roberto Casadei
  *
  */
trait SensorDefinitions { self: AggregateProgram =>
  def sense1 = sense[Boolean](SensorEnum.SENS1.name)
  def sense2 = sense[Boolean](SensorEnum.SENS2.name)
  def sense3 = sense[Boolean](SensorEnum.SENS3.name)
  def sense4 = sense[Boolean](SensorEnum.SENS4.name)
  def sense5 = sense[Boolean](SensorEnum.SENS5.name)
  def sense6 = sense[Boolean](SensorEnum.SENS6.name)
  def sense7 = sense[Boolean](SensorEnum.SENS7.name)
  def nbrRange() = nbrvar[Double](NBR_RANGE_NAME) * 100
}
