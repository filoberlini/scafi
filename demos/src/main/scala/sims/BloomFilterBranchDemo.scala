package sims

import java.text.SimpleDateFormat
import java.util.Calendar

import it.unibo.scafi.simulation.gui.{Launcher, Settings}
import lib.{BlockC, BlockG, BlockT2, SensorDefinitions}
import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import it.unibo.scafi.simulation.gui.SettingsSpace.Topologies
import it.unibo.scafi.simulation.gui.model.implementation.SensorEnum


/**
  * Created by Filippo Berlini on 28/08/2017.
  */

object BloomFilterBranchDemo extends Launcher {
  // Configuring simulation
  Settings.Sim_ProgramClass = "sims.BloomFilterBranch" // starting class, via Reflection
  Settings.ShowConfigPanel = false // show a configuration panel at startup
  Settings.Sim_Topology = Topologies.Grid_LoVar
  Settings.Sim_NbrRadius = 0.10 // neighbourhood radius
  Settings.Sim_NumNodes = 300 // number of nodes
  Settings.Gradient_Orientation_Activator = (i: Any) => i.asInstanceOf[Int]
  Settings.Led_Activator = (b: Any) => b.asInstanceOf[Boolean]
  Settings.To_String = (b: Any) => ""
  Settings.Path_Activator = true
  launch()
}

class BloomFilterBranch extends AggregateProgram with SensorDefinitions with BlockG with BlockT2 with BlockC{

  def range(passingTimeRange: (Int,Int), distance: Int):Boolean  = {
    val currentTime = Calendar.getInstance().getTime()
    val minuteFormat = new SimpleDateFormat("mm")
    val hourFormat = new SimpleDateFormat("hh")
    val currentHour = hourFormat.format(currentTime).toInt
    val currentMinute = minuteFormat.format(currentTime).toInt
    val myPassingTime = distance + (currentMinute * 60) + (currentHour * 3600)
    return myPassingTime < passingTimeRange._1 | myPassingTime > passingTimeRange._2
  }

  def mockFilterEvaluation(filter: Map[Boolean, (Int,Int)]): (Int,Int) = {
    var myFilter = filter
    def isObstacle1 = sense[Boolean](SensorEnum.SENS3.name)
    def isObstacle2 = sense[Boolean](SensorEnum.SENS4.name)
    def isObstacle3 = sense[Boolean](SensorEnum.SENS5.name)
    def isObstacle4 = sense[Boolean](SensorEnum.SENS6.name)
    def isObstacle5 = sense[Boolean](SensorEnum.SENS7.name)
    val currentTime = Calendar.getInstance().getTime()
    val minuteFormat = new SimpleDateFormat("mm")
    val hourFormat = new SimpleDateFormat("hh")
    val currentHour = hourFormat.format(currentTime).toInt
    val currentMinute = minuteFormat.format(currentTime).toInt
    val passingTime = (currentMinute * 60) + (currentHour * 3600)
    mux(isObstacle1){
      myFilter +=(isObstacle1 -> (filter.apply(isObstacle1)._1+passingTime,filter.apply(isObstacle1)._2+passingTime))
      myFilter.apply(isObstacle1)
    } {
      mux(isObstacle2) {
        myFilter +=(isObstacle2 -> (filter.apply(isObstacle2)._1+passingTime,filter.apply(isObstacle2)._2+passingTime))
        myFilter.apply(isObstacle2)
      } {
        mux(isObstacle3) {
          myFilter +=(isObstacle3 -> (filter.apply(isObstacle3)._1+passingTime,filter.apply(isObstacle3)._2+passingTime))
          myFilter.apply(isObstacle3)
        } {
          mux(isObstacle4) {
            myFilter +=(isObstacle4 -> (filter.apply(isObstacle4)._1+passingTime,filter.apply(isObstacle4)._2+passingTime))
            myFilter.apply(isObstacle4)
          } {
            mux(isObstacle5) {
              myFilter +=(isObstacle5 -> (filter.apply(isObstacle5)._1+passingTime,filter.apply(isObstacle5)._2+passingTime))
              myFilter.apply(isObstacle5)
            } {
              (0,0)
            }
          }
        }
      }
    }
  }

  def bloomFilterGradient2(source: Boolean, target: Boolean, bound: Int, filter: Map[Boolean, (Int,Int)]): Double =
    branch(range(mockFilterEvaluation(filter), distanceTo(source).toInt)){
      distanceTo(target)
    }{Double.MaxValue}

  def bloomFilterGradient(source: Boolean, bound: Int, filter: Map[Boolean, (Int,Int)]): Double = {
    branch(range(mockFilterEvaluation(filter), distanceTo(source).toInt)){
      distanceTo(source)
    }{Double.MaxValue}
  }

  def filterDistanceTo(source: Boolean,  bound: Int, filter: Map[Boolean, (Int,Int)], default: Double): Double =
    rep(Double.MaxValue) { case (dist) =>
      mux(source) {  0.0  } {
        val distance = minHoodPlus { nbr{dist} + nbrRange() }
        mux(range(mockFilterEvaluation(filter), distance.toInt)){
          distance
        }{default}
      }
    }

  def shortestPath(source: Boolean, gradient: Double): Boolean =
  rep(false)(
    path => mux(source){
      true
    } {
      foldhood(false)(_|_) {
        nbr(path) & gradient == nbr(minHood(nbr(gradient)))
      }
    }
  )

  def shortestPath2(source: Boolean, gradient: Double): Boolean =
    rep(false)(
      path => mux(source){
        true
      } {
        foldhood(false)(_|_) {
          nbr(path) & mid() == nbr(findParent(gradient))
        }
      }
    )

  override def main() = {
    var filter = Map[Boolean,(Int,Int)]()
    filter += (sense3->(20,40))
    filter += (sense4->(40,60))
    filter += (sense5->(60,80))
    filter += (sense6->(80,100))
    filter += (sense7->(100,120))
    //shortestPath(sense2, bloomFilterGradient(sense1, 20, filter))
    shortestPath(sense2, filterDistanceTo(sense1, 20, filter, Double.MaxValue))
    //shortestPath(sense2, bloomFilterGradient2(sense2, sense1, 20, filter))
    //val gradient = bloomFilterGradient(sense1, 20, filter)
    //val gradient = bloomFilterGradient2(sense2, sense1, 20, filter)
    /*val gradient = filterDistanceTo(sense1, 20, filter)
    rep(false, 0){case(_,_)=>
      (shortestPath(sense2, gradient),gradient.toInt)
    }*/
    //findParent(filterDistanceTo(sense1, 20, filter))
  }
}
