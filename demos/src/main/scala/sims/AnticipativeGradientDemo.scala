package sims

import it.unibo.scafi.simulation.gui.{Launcher, Settings}
import lib._
import it.unibo.scafi.incarnations.BasicSimulationIncarnation._
import Builtins._
import it.unibo.scafi.simulation.gui.SettingsSpace.Topologies

/**
  * Created by Filippo Berlini on 12/07/2017.
  */
object AnticipativeGradientDemo extends Launcher {
  // Configuring simulation
  Settings.Sim_ProgramClass = "sims.AnticipativeGradient" // starting class, via Reflection
  Settings.ShowConfigPanel = false // show a configuration panel at startup
  Settings.Sim_NbrRadius = 0.08 // neighbourhood radius
  Settings.Sim_Topology = Topologies.Grid
  Settings.Sim_NumNodes = 400 // number of nodes
  Settings.To_String = (b: Any) => ""
  Settings.Gradient_Orientation_Activator = (i: Any) => i.asInstanceOf[Int]
  Settings.Path_Activator = true;
  Settings.Led_Activator = (b: Any) => b.asInstanceOf[Boolean]
  launch()
}

class AnticipativeGradient extends AggregateProgram with SensorDefinitions with BlockG with BlockT2 with BlockC{

  def horizonWave (obstacle: Boolean, tS: Double, tE: Double): Boolean ={
    val distanceObst = distanceTo(obstacle)
    distanceObst >= tS & distanceObst <= tE
  }
  def gradientShadow (target: Boolean, obstacle: Boolean): Boolean =
    G[Double](target, 0, x=> if(obstacle) x+1 else x , nbrRange()) > 0

  def futureEventWarning (target: Boolean, obstacle: Boolean, tS: Double, tE: Double): Boolean =
    horizonWave(obstacle, tS, tE) & gradientShadow(target, obstacle)

  def functionStay (target: Boolean, obstacle: Boolean, tE: Double): Double =
    distanceTo(target)+ tE - distanceTo(obstacle)

  def functionChange (target: Boolean, obstacle: Boolean): Double =
    branch(obstacle) {Double.MaxValue}{distanceTo(target)}

  def anticipativeGradient (target: Boolean, obstacle: Boolean, tS: Double, tE: Double): Double = {
    mux(futureEventWarning(target, obstacle, tS, tE)) {
      functionStay(target, obstacle, tE) min functionChange(target, obstacle)
    } {
      (distanceTo(target))
    }
  }


  override def main() = {
    //horizonWave(sense1, 0, 10)                                                     //horizonWave test
    //gradientShadow(sense1, sense2)                                                  //gradientShadow test
    //futureEventWarning(sense1, sense2, 10, 30)                                      //futureEventWarning test
    /*rep(0.0, 0.0, 0.0) { case (_, _, _) =>                                          //functionStay test
      (distanceTo(sense1), distanceTo(sense2), functionStay(sense1, sense2,100))
    }*/
    /*rep(0.0, 0.0) { case (_, _) =>                                                  //functionChange test
      (distanceTo(sense1), functionChange(sense1, sense2))
    }*/
    /*rep(0.0, 0.0) { case (_, _) =>                                                  //functionStay and functionChange test
      (functionStay(sense1, sense2, 50), functionChange(sense1, sense2))
    }*/
    /*rep(0.0, 0.0) { case (_, _) =>                                                  //functionStay and functionChange and futureEventWarning test
      mux(futureEventWarning(sense1, sense2, 10, 50)) {
        (functionStay(sense1, sense2, 50), functionChange(sense1, sense2))
      }{(0.0,0.0)}
    }*/
    /*val fc = functionChange(sense1, sense2)
    val fs = functionStay(sense1, sense2, 50)
    rep(0.0, 0.0) { case (_, _) =>                                                  //anticipativeGradient test
      mux(futureEventWarning(sense1, sense2, 10, 50)) {                             //0.0 it means change path
        (fs min fc, if(fs<fc) 1.0 else 0.0)                                         //1.0 it means stay in the path
      }{(distanceTo(sense1),2.0)}                                                   //2.0 it means out of warning range
    }*/
    findParent(anticipativeGradient(sense1, sense2, 10, 40))                        //antcipativeGradient test with orientation
  }
}
