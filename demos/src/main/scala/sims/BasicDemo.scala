package sims

import it.unibo.scafi.incarnations.BasicSimulationIncarnation.AggregateProgram
import it.unibo.scafi.simulation.gui.{Launcher, Settings}
import lib.{BlockC, BlockG, SensorDefinitions}

/**
  * @author Roberto Casadei
  *
  */
object BasicDemo extends Launcher {
  // Configuring simulation
  Settings.Sim_ProgramClass = "sims.BasicProgram" // starting class, via Reflection
  Settings.ShowConfigPanel = false // show a configuration panel at startup
  Settings.Sim_NbrRadius = 0.15 // neighbourhood radius
  Settings.Sim_NumNodes = 100 // number of nodes
  launch()
}

class BasicProgram extends AggregateProgram with SensorDefinitions with BlockG{
  override def main() = G2(sense1)(0.0)(_ + nbrRange)() // the aggregate program to run
}